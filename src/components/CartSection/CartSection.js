/** @format */

import React from "react";
import "./CartSection.css";
import clothes02 from "../../assets/clothes02.jpeg";
import clothes01 from "../../assets/clothes01.jpeg";

const CartSection = (props) => {
  return (
    <div className="row cart-section-holder">
      <div className="col-12 py-4 border-bottom ">
        <div className="order-text">Your Order</div>
      </div>
      <div className="col-12 border-bottom py-4">
        <div className="d-flex">
          <div className="icon-container-bg bg-light">
            <img src={clothes02} className="clothes02" alt="clothes" />
          </div>
          <div className="ms-4 d-flex flex-column">
            <div className="item-name">Jeans with Sequins</div>
            <div className="d-flex mt-1">
              <div className="item-size ">
                Size<span className="main-item ">XL</span>
              </div>
              <div className="item-size ms-2">
                Color<span className="main-item ">Blue</span>
              </div>
            </div>
            <div className="d-flex align-items-center mt-1">
              <div className="amount">$39,00 </div>
              <div className="mx-2 seperator">X</div>
              <div className="last-number">02</div>
            </div>
          </div>
        </div>
      </div>

      <div className="col-12 border-bottom py-4">
        <div className="d-flex">
          <div className="icon-container-bg bg-light">
            <img src={clothes01} className="clothes01" alt="clothes" />
          </div>
          <div className="ms-4 d-flex flex-column">
            <div className="item-name">Robinson Printed</div>
            <div className="d-flex mt-1">
              <div className="item-size ">
                Size<span className="main-item ">XXL</span>
              </div>
              <div className="item-size ms-2">
                Color<span className="main-item ">Blue</span>
              </div>
            </div>
            <div className="d-flex align-items-center mt-1">
              <div className="amount">$29,00 </div>
              <div className="mx-2 seperator">X</div>
              <div className="last-number">01</div>
            </div>
          </div>
        </div>
      </div>

      <div className="col-12 py-4">
        <div className="d-flex align-items-center ">
          <div className="delivery">Delivery</div>
          <div className="delivery-price custom-spacing">
            $20<span className="express-text">(Express)</span>
          </div>
        </div>
        <div className="d-flex align-items-center mt-4 ">
          <div className="delivery">Discount</div>
          <div className="delivery-price custom-spacing">-$10</div>
        </div>
      </div>
      <div className="col-12 py-4 bg-light">
        <div className="d-flex align-items-center">
          <div className="total-text">Total</div>
          <div className="total-amount custom-spacing ">$117.00</div>
        </div>
      </div>
    </div>
  );
};

export default CartSection;
