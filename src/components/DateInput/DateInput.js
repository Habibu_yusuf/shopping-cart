/** @format */

import React, { useRef } from "react";
import "./DateInput.css";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import ArrowUp from "../../assets/triangle-up.svg";
import ArrowDown from "../../assets/triangle-down.svg";
import moment from "moment";

const DateInput = ({ startDate, setStartDate, setIsChecked, isChecked }) => {
  const dateRef = useRef();

  const handleChange = (date) => {
    setStartDate(date);
    if (moment(date).format("MM/YYYY")) {
      setIsChecked(true);
    }
  };

  const openDatePicker = () => {
    dateRef.current.onInputClick();
  };
  return (
    <div>
      <div onClick={openDatePicker} className="custom-date-holder">
        {isChecked && (
          <div className="d-flex align-items-center checked justify-content-center inner-holder">
            {moment(startDate).format("MM/YYYY")}
          </div>
        )}
        {!isChecked && (
          <div className="inner-holder">
            <div className="d-flex align-items-center">
              <div className="date-items">MM</div>
              <img src={ArrowUp} className="date-icon" alt="ArrowUp" />
            </div>
            <div className="ms-auto d-flex align-items-center">
              <div className="date-items">YYYY</div>
              <img src={ArrowDown} className="date-icon" alt="ArrowDown" />
            </div>
          </div>
        )}
      </div>
      <DatePicker
        ref={dateRef}
        className="Date-picker"
        selected={startDate}
        onChange={handleChange}
        onSelect={handleChange}
        dateFormat="MM/yyyy"
        showMonthYearPicker
      />
    </div>
  );
};

export default DateInput;
