/** @format */

import React, { useState } from "react";
import CartSection from "../CartSection/CartSection";
import Header from "../Header/Header";
import ShoppingSection from "../ShoppingSection/ShoppingSection";
import "./Home.css";

const Home = (props) => {
  const [startDate, setStartDate] = useState(new Date());
  const [isChecked, setIsChecked] = useState(false);

  const [c_Value, setC_Value] = useState("");
  const [c_Number, setC_Number] = useState("");
  const [error, setError] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [dateError, setDateError] = useState("");
  return (
    <div className="container-xxl d-flex flex-column justify-content-center py-5">
      <Header />

      <div className="row pt-5 px-2 justify-content-between">
        <div className="col-8 shopping-cart-section">
          <ShoppingSection
            dateError={dateError}
            errorMessage={errorMessage}
            error={error}
            c_Value={c_Value}
            c_Number={c_Number}
            setC_Value={setC_Value}
            setC_Number={setC_Number}
            setError={setError}
            setErrorMessage={setErrorMessage}
            setDateError={setDateError}
            setStartDate={setStartDate}
            startDate={startDate}
            setIsChecked={setIsChecked}
            isChecked={isChecked}
          />
        </div>
        <div className="col-3 cart-section-container border">
          <CartSection />
        </div>
      </div>
    </div>
  );
};

export default Home;
