/** @format */

import React from "react";
import CheckIcon from "../../assets/check.svg";
import debitCard from "../../assets/debitCard.png";
import wallet from "../../assets/wallet.png";
import web from "../../assets/web.png";
import "./ShoppingSection.css";
import Button from "../Button/Button";
import DateInput from "../DateInput/DateInput";
import moment from "moment";

const ShoppingSection = ({
  c_Value,
  c_Number,
  dateError,
  error,
  errorMessage,
  setError,
  setC_Value,
  setC_Number,
  setErrorMessage,
  setDateError,
  startDate,
  setStartDate,
  isChecked,
  setIsChecked,
}) => {
  const handleSubmit = (e) => {
    e.preventDefault();

    if (!c_Value || c_Value.length < 3) {
      setErrorMessage("Number must be 3 digits");
    }

    if (!c_Number || c_Number.length < 16) {
      setError("Number must be 16 digits");
    }

    if (!startDate) {
      setDateError("Select Date");
    }

    if (c_Value.length === 3 && c_Number.length === 16 && startDate) {
      const myObj = {
        "Card Number": c_Number,
        "CVV Number": c_Value,
        "Valid Date": moment(startDate).format("MM/YYYY"),
      };
      alert(JSON.stringify(myObj, null, 4));
      setC_Value("");
      setC_Number("");
      setIsChecked(false);
    }
  };

  const customLetter = {
    backgroundColor: "#fff",
    color: "#000",
    border: "1px solid gray",
  };

  return (
    <div className="row">
      <div className="col-12 shadow px-4 py-4 custom-border">
        <div className="d-flex  container-response">
          <div className="payment-b custom-payment-top" style={customLetter}>
            a
          </div>
          <div className="d-flex flex-column  custom-col ">
            <div className="d-flex ">
              <div className="Login mb-2">LOGIN</div>
              <img
                src={CheckIcon}
                className="check-icon ms-3 mt-1"
                alt="CheckIcon"
              />
            </div>
            <div className="d-flex">
              <div className="Name">Micheal Smith</div>
              <div className="ms-4 Number">+806 -445 -4453</div>
            </div>
          </div>
          <Button
            title={"CHANGE"}
            customClass="custom-change-button"
            handleClick={() => console.log("changed")}
          />
        </div>
      </div>

      <div className="col-12 mt-3  shadow px-4 py-4 custom-border">
        <div className="d-flex container-response">
          <div className="payment-b custom-payment-top" style={customLetter}>
            b
          </div>
          <div className="d-flex flex-column custom-col">
            <div className="d-flex ">
              <div className="Login mb-2">SHIPPING ADDRESS</div>
              <img
                src={CheckIcon}
                className="check-icon ms-3 mt-1"
                alt="CheckIcon"
              />
            </div>
            <div className="d-flex">
              <div className="d-flex flex-column">
                <div className="Name mb-2">
                  Brandy Cooper, New Civil Colony, Salt Lake City, Utah
                </div>
                <div className="Name">United States, 2971 Avenue</div>
              </div>
            </div>
          </div>

          <Button
            title={"CHANGE"}
            customClass="custom-change-button"
            handleClick={() => console.log("clicked")}
          />
        </div>
      </div>
      <div className="col-12 bg-light py-4 mt-4 payment-method-container ">
        <div className="d-flex ">
          <div className="payment-b">b</div>
          <div className="payment-method ms-5">PAYMENT METHOD</div>
        </div>
      </div>
      <div className="col-12 mt-3 ">
        <div className="d-flex credit-section w-100 ">
          <div className="payment-b custom-payment-icon mt-3"></div>
          <div className="form-section d-flex flex-column w-100 ">
            <div className="d-flex align-items-center ">
              <div className="icon-container">
                <img
                  src={debitCard}
                  className="debitCard-icon"
                  alt="debitCard"
                />
              </div>

              <div className="ms-4 debitcard-text">Debit / Credit Card</div>
            </div>
            <form onSubmit={handleSubmit}>
              <div className="mt-3 row">
                <div className="col-12 card-number-section">
                  <label
                    htmlFor="exampleFormControlInput1"
                    className="form-label custom-label"
                  >
                    Enter Card Number *
                  </label>
                  <input
                    value={c_Number}
                    onChange={(e) => setC_Number(e.target.value)}
                    type="text"
                    className="form-control custom-input"
                    id="exampleFormControlInput1"
                    maxLength="16"
                    required
                  />
                  {error && <div className="hasError">{error}</div>}
                </div>
              </div>
              <div className="row mt-3">
                <div className="col-3 c-number-section">
                  <label
                    htmlFor="exampleFormControlInput1"
                    className="form-label custom-label"
                  >
                    Valid Date
                  </label>
                  <DateInput
                    startDate={startDate}
                    isChecked={isChecked}
                    setIsChecked={setIsChecked}
                    setStartDate={setStartDate}
                  />

                  {dateError && <div className="hasError">{dateError}</div>}
                </div>
                <div className="col-3 c-number-section">
                  <label
                    htmlFor="exampleFormControlInput1"
                    className="form-label custom-label"
                  >
                    CVV *
                  </label>
                  <input
                    value={c_Value}
                    onChange={(e) => setC_Value(e.target.value)}
                    type="text"
                    className="form-control myInput"
                    id="exampleFormControlInput1"
                    maxLength="3"
                    required
                  />
                  {errorMessage && (
                    <div className="hasError">{errorMessage}</div>
                  )}
                </div>
                <div className="col-4 c-number-section custom-pay-button align-self-center">
                  <Button
                    title={"Pay $117.00"}
                    customClass="pay-custom-button"
                    type="submit"
                  />
                </div>
              </div>
            </form>
            <div className="row my-4">
              <div className="col-12">
                <div className="card-text">
                  Your card details would be securely saved for faster payments.
                  your CVV will not be stored
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-12 mb-4">
        <div className="d-flex">
          <div
            className="payment-b banking-section mt-3"
            style={customLetter}
          ></div>
          <div className="d-flex align-items-center custom-col ">
            <div className="icon-container">
              <img src={web} className="wallet-icon" alt="webIcon" />
            </div>

            <div className="ms-4 debitcard-text">Net Banking</div>
          </div>
        </div>
      </div>
      <div className="col-12">
        <div className="d-flex ">
          <div
            className="payment-b banking-section mt-3"
            style={customLetter}
          ></div>
          <div className="d-flex align-items-center custom-col ">
            <div className="icon-container">
              <img src={wallet} className="wallet-icon" alt="wallet" />
            </div>
            <div className="ms-4 debitcard-text">Google / Apple Wallet</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ShoppingSection;
