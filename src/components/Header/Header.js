/** @format */

import React from "react";
import "./Header.css";

const Header = (props) => {
  return (
    <div>
      <h2 className="header-title pt-5">Shopping Cart</h2>
      <div className="mt-3">
        <ul className="d-flex list-unstyled">
          <li className="me-2">
            <a className="list-item" href="#">
              Homepage /
            </a>
          </li>
          <li className="me-2">
            <a className="list-item" href="#">
              Clothing Categories /
            </a>
          </li>
          <li className="">
            <a className="last-item" href="#">
              My shopping Cart
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Header;
