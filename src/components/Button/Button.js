/** @format */

import React from "react";

const Button = ({ title, CustomStyle, handleClick, customClass }) => {
  return (
    <button className={customClass} onClick={handleClick} style={CustomStyle}>
      {title}
    </button>
  );
};

export default Button;
